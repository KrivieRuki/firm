from django.contrib import admin

from .models import Firm, Employee

admin.site.register(Firm)
admin.site.register(Employee)
