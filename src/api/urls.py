from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from .views import FirmViewSet


router = routers.DefaultRouter()
router.register(r'firms', FirmViewSet)

urlpatterns = [
    path('', include(router.urls))
]