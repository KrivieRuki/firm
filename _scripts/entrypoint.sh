#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $POSTGRES_HOST $POSTGRES_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

cd src
python manage.py flush --no-input
python manage.py migrate
python manage.py collectstatic --no-input

gunicorn firm.wsgi:application --bind 0.0.0.0:8000
